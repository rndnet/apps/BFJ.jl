# Rndnet job helper functions for Julia

```jl
Pkg.clone("https://gitlab.com/rndnet/apps/BFJ.jl.git")
```
or

```bash
julia -E 'import Pkg; Pkg.add(Pkg.PackageSpec(url="https://gitlab.com/rndnet/apps/BFJ.jl", rev="master"))'
```
